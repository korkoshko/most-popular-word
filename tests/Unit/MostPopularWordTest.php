<?php

use PHPUnit\Framework\TestCase;
use Korkoshko\MostPopular\MostPopular;
use Korkoshko\MostPopular\MostPopularWord;

final class MostPopularWordTest extends TestCase
{
    private MostPopular $mostPopularWord;

    public function setUp(): void
    {
        parent::setUp();

        $this->mostPopularWord = new MostPopularWord();
    }

    public function testFindMostPopularWordInText(): void
    {
        $text = 'The find most popular word in text the';

        $this->assertSame($this->mostPopularWord->find($text), 'the');
    }

    public function testFindMostPopularWordInEmptyText(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Text is empty');

        $text = '';

        $this->mostPopularWord->find($text);
    }
}

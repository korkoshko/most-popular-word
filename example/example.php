<?php

require_once __DIR__ . '/../vendor/autoload.php';

const MAX_CHARS = 10000;

$faker = Faker\Factory::create();

$text = $faker->realText(MAX_CHARS);

echo 'Text: ' . $text . PHP_EOL;

$mostPopularWord = new \Korkoshko\MostPopular\MostPopularWord();

echo PHP_EOL . 'Most popular word: ' . $mostPopularWord->find($text) . PHP_EOL;

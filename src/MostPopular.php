<?php

namespace Korkoshko\MostPopular;

interface MostPopular
{
    public function find(string $text): string;
}

<?php

namespace Korkoshko\MostPopular;

final class MostPopularWord implements MostPopular
{
    private const ONLY_WORD = 1;

    public function find(string $text): string
    {
        $cleanText = $this->process($text);

        if (! $cleanText) {
            throw new \InvalidArgumentException('Text is empty');
        }

        $aggregateWords = array_count_values(
            str_word_count($cleanText, self::ONLY_WORD)
        );

        arsort($aggregateWords);

        return array_key_first($aggregateWords);
    }

    private function process(string $text): string
    {
        return mb_strtolower(trim($text));
    }
}
